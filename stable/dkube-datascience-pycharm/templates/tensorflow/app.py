from __future__ import print_function
import pydevd_pycharm
import argparse
import tensorflow as tf


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')

    args = parser.parse_args()

    if args.debug:
        print ("Debugger: waiting...")
        ## CHANGE_ME: replace 'localhost' with host IP
        pydevd_pycharm.settrace('localhost', port=5679, stdoutToServer=True, stderrToServer=True)
        

    # Simple hello world using TensorFlow

    # Create a Constant op
    # The op is added as a node to the default graph.
    #
    # The value returned by the constructor represents the output
    # of the Constant op.
    hello = tf.constant('Hello, TensorFlow!')

    # Start tf session
    sess = tf.Session()

    # Run the op
    print(sess.run(hello))