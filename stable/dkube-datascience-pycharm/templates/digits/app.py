import pydevd_pycharm
import argparse
import os, sys
import shutil

sys.path.insert(0, "/project/user-app/classifier/program")

os.environ["DATUMS_PATH"] = "/project/user-app/classifier/data"
os.environ["DATASET_NAME"] = "mnist"
if os.environ.get("OUT_DIR", "") == "":
    os.environ["OUT_DIR"] = "/tmp/out"
    shutil.rmtree("/tmp/out", ignore_errors=True)
    
import model


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')

    args = parser.parse_args()

    if args.debug:
        print ("Debugger: waiting...")
        ## CHANGE_ME: replace 'localhost' with host IP
        pydevd_pycharm.settrace('localhost', port=5679, stdoutToServer=True, stderrToServer=True)


    model.run()
