
#### Compose a cloud native masterpiece.

Infused with cloud native capabilities from the moment you start, Appsody provides everything you need to iteratively develop applications, ready for deployment to Kubernetes environments. Teams are empowered with sharable technology stacks, configurable and controllable through a central hub.

# Stacks

This respository holds the default set of stacks available from One Convergence. Stacks include a base container image and project templates which act as a starting point for your application development.

Stacks allow for rapid development whilst giving the stack developer the ability to control the overall applications that are created from it.

To find out more about Appsody check out [appsody.dev](https://appsody.dev).



## Creating a Stack
The stack image contains everything that will be common throughout all templates that leverage it. 

The `/image` directory will contain everything that is needed for the stack's image. You **must** include a `Dockerfile-stack` file in the `/image` directory, which defines how the stack image is built.

Stack creators configure enviroment variables in `Dockerfile-stack` to specify the behaviour they expect from the stack throughout the application development lifecycle. `Appsody CLI` and `Appsody controller` inspect these environment variables and then drive the expected behaviour for the developer.

The `/image/project` directory contains the base of the application. You may decide not to include any application code here but it is recommended to add some value to the stack. For example, by controlling dependency versions. The `project` **must** include a production `Dockerfile` here which will be used by the `appsody build` command.

Once you have created above files, run docker build and push command to generate image for the stack. 

```
docker build -f Dockerfile-stack -t rjgybxss/dkube-datascience-rstudio:v1.13 .
docker push rjgybxss/dkube-datascience-rstudio:v1.13
```

Example Stack: [Datascience for PyCharm](/stable/dkube-datascience-pycharm/image)

## Creating a template
Templates provide an initial application to enable developers to get started with a stack. They provide a starter application that a developer can expand and adapt as they require.

All templates should be created within `/templates`. Every template is contained within its own directory, `/templates/<template-name>`.

Each template must contain `appsody-config.yaml` to specify what stack image the template will use. For example:
```
stack: <org-name>/<stack-id>
```

Template can be customized for a paricular IDE by adding additional files (.vscode for VSCode, .idea for PyCharm and R scripts for RStudio)

Once template is created, you can tar the template and host it on a webserver. 

Example template: [PyCharm Digits](/stable/dkube-datascience-pycharm/templates/digits)

## Creating a repository

To create a new repository, create a YAML file similar to [index.yaml] (/index.yaml) in this repo. You can add your repository by running:
```
appsody repo add <name> <index-url>
```
